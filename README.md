# Diary++
This project will be depricated after the development of Propel
 - [Github](https://github.com/RikilG/ProPl/)
 - [Gitlab](https://gitlab.com/RikilG/propel)

This is a desktop application build with electron framework to manage your diary entries. This is still in early development stage and has a long way to go...!

## To Run

```sh
git clone https://github.com/RikilG/Diary-Plus-Plus.git
cd Diary-Plus-Plus
npm start
```

## Features: (checked are completed)
 - [x] Maintain multiple diaries
 - [x] Have Encrypted Diary entries(just to prevent direct read/write access to files)
 - [x] Import entries from text file
 - [ ] Have password protected diary
 - [ ] Option to encrypt whole diary and decrypt whole diary.
 - [ ] Sync diaries with cloud like drive or dropbox
 - [ ] List and search diary entries
 - [ ] Export to md, html, pdf, etc...
 - [ ] Preferences(diary save location and other settings)
 - [ ] Page to edit preferences

## Optional: (might implement after major work is done)
 - [ ] Refine editor as a code editor
 - [ ] Option to switch to Dark mode or Light mode
 - [ ] vim style key-bindings

There might be more features added to this application in the future.

More features and contributions are welcome!